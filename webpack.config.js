const path = require( 'path' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );

module.exports = {
	mode: 'development',
	entry: {
		index: {
			import: './src/index.ts'
			// dependOn: 'shared'
		}
		// print: {
		// 	import: './src/print.js',
		// 	dependOn: 'shared'
		// },
		// load: {
		// 	import: './src/load.js',
		// 	dependOn: 'shared'
		// },
		// shared: 'lodash'
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve( __dirname, 'dist' ),
		clean: true
	},
	optimization: {
		runtimeChunk: 'single'
		// splitChunks: {
		// 	chunks: 'all'
		// }
	},
	devtool: 'inline-source-map',
	// This tells webpack-dev-server to serve the files from the dist directory on localhost:8080.
	devServer: {},
	plugins: [
		new HtmlWebpackPlugin( {
			title: 'Some fun with Typescript'
		} )
	],
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.(png|svg|jpg|jpeg|gif)$/i,
				type: 'asset/resource'
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/i,
				type: 'asset/resource'
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [ '.tsx', '.ts', '.js', '.css' ]
	}
};
