import { CE } from '../services/tools';
import { printMe } from './print';

export default function load () {
	const element = CE( 'div' );
	const btn = CE( 'button' );
	const wrap = CE( 'div' );
	const btnwrap = CE( 'div' );

	wrap.classList.add( 'wrap' );
	btnwrap.classList.add( 'btnwrap' );
	btn.classList.add( 'btn' );

	element.classList.add( 'element' );

	btn.innerHTML = 'Create table !';
	btn.onclick = () => printMe( 'table' );

	btnwrap.appendChild( btn );
	element.appendChild( btnwrap );
	element.appendChild( wrap );

	return element;
}
