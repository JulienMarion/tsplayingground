import { store } from '../redux/store';
import Table from '../services/class/Table';
import { getItemsFromAPI, QS, selectEntities } from '../services/tools';

export async function printMe ( identifier: string ): Promise<void> {

	if ( QS( `#${ identifier }` ) ) Table._deleteTable( identifier );

	getItemsFromAPI();

	const table = new Table();

	table._createTable( {
		elder: QS( `.wrap` ),
		datas: await selectEntities( store.getState() ), // yes await has effect..
		identifier
	} );

}
