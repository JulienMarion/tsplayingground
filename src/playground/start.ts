import load from './load';

export default async function start () {
	return load();
}
