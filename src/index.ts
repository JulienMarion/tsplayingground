import './styles/config';
import start from '../src/playground/start';

document.addEventListener( 'DOMContentLoaded', () => {
	start()
		.then( ( app ) => {
			document.body.append( app );
		} );
} );
