import { store } from '../redux/store';
import { getItems } from '../redux/actions';
import { InitialState } from '../redux/reducers';

export const QS = ( selector: string ): Element => document.querySelector( selector );

export const CE = ( type: string ): HTMLElement => document.createElement( type );

export const getItemsFromAPI = (): {} => store.dispatch( getItems() );

export const selectEntities = ( state: InitialState ) => state.items;

export const selectedItem = ( state: InitialState ) => state.selectedItem;

export const currentItem = ( state: InitialState ) => state.item;
