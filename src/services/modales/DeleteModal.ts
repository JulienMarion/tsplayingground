import { Modal } from './Modal';
import { CE, QS } from '../tools';
import { store } from '../../redux/store';
import { deleteItem } from '../../redux/actions';
import { selectedItem } from '../tools';

class DeleteModal extends Modal {
	private static instance: DeleteModal;
	public type: string = 'DeleteModal';

	private constructor( parent: Element ) {
		super( parent );
	}

	public close (): void {
		this.fog.remove();
		this.frame.remove();
	}

	public open (): void {
		this.fog = CE( 'div' );
		this.fog.classList.add( 'modalFog' );

		const frame = CE( 'div' );

		frame.classList.add( this.type );
		frame.classList.add( 'modal' );

		this.parent.appendChild( this.fog );
		this.parent.appendChild( frame );
		this.frame = frame;
		this._loadFrame();
	}

	public static getInstance () {

		if ( !this.instance ) { return new DeleteModal( document.body ); }

		return this.instance;
	}

	private _loadFrame (): void {
		this._createWrap( 'wrapContent', this.frame );
		this._content( 'modalContent', 'Êtes-vous sûr?!' );
		this._createWrap( 'wrapBtn', this.frame );
		this._createBtn( 'noBtn', 'No !', () => this.close() );

		this._createBtn( 'okBtn', 'Ok !', () => {
			store.dispatch( deleteItem( selectedItem( store.getState() ) ) );
			this.close();
		} );
	}

	protected _content ( htmlclass: string, content: string ) {
		const wrap = QS( `.wrapContent` );
		const p = CE( 'p' );

		p.classList.add( htmlclass );
		p.innerHTML = content;

		wrap.appendChild( p );
	}
}

export default DeleteModal.getInstance();
