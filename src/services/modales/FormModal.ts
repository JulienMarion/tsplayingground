import { Modal } from './Modal';
import { store } from '../../redux/store';
import { getFullItem, updateItem } from '../../redux/actions';
import { selectedItem, currentItem } from '../tools';
import { Form } from '../class/Form';
import { CE, QS } from '../tools';

class FormModal extends Modal {
	private static instance: FormModal;
	public type: string = 'FormModal';
	public form: any;

	constructor( parent: Element ) {
		super( parent );
	}

	public open (): void {
		this.fog = CE( 'div' );
		this.fog.classList.add( 'modalFog' );

		const frame = CE( 'div' );

		frame.classList.add( this.type );
		frame.classList.add( 'modal' );

		this.parent.appendChild( this.fog );
		this.parent.appendChild( frame );
		this.frame = frame;
		this._loadFrame();
	}

	public close (): void {
		this.fog.remove();
		this.frame.remove();
	}

	private async _loadFrame (): Promise<void> {
		this._createWrap( 'wrapContent', this.frame );
		this._content();
		this._createWrap( 'wrapBtn', this.frame );
		this._createBtn( 'noBtn', 'No !', () => this.close() );

		this._createBtn( 'okBtn', 'Enregistrer !', () => {
			store.dispatch(
				updateItem( selectedItem( store.getState() ), this.createObjectFromform() )
			);
			this.close();
		} );
	}

	private async _generateForm ( parent: Element ) {
		const itemID = await selectedItem( store.getState() );

		await store.dispatch( getFullItem( itemID ) );

		const data = await currentItem( store.getState() );
		const form = new Form( parent, data );

		this.form = form._generateForm();

		return this.form;
	}

	protected _content () {
		const wrap = QS( `.wrapContent` );

		this._generateForm( wrap );
	}

	public static getInstance () {

		if ( !this.instance ) { return new FormModal( document.body ); }

		return this.instance;
	}

	private createObjectFromform () {
		const inputs = document.querySelectorAll( 'form .formControl input' );
		const keys = Array.from( inputs ).map( input => input.getAttribute( 'id' ) );

		return Object.fromEntries(
			keys.map( ( _, i ) => [
				keys[ i ],
				Array.from( inputs as NodeListOf<HTMLInputElement> )
					.map( input => input.getAttribute( 'value' ) || input.checked )[ i ]
			] )
		);
	}
}

export default FormModal.getInstance();
