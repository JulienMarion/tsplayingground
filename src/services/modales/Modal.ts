import { CE, QS } from '../tools';

export abstract class Modal {
	public fog: Element;
	public frame: Element;
	public type: string;

	constructor( public parent: Element ) { }

	abstract close (): void;

	abstract open (): void;

	protected abstract _content ( htmlclass?: string, content?: string ): void;

	protected _createBtn ( htmlclass: string, title: string, callback: any ): void {
		const wrap = QS( `.wrapBtn` );
		const btn = CE( 'btn' );

		btn.innerHTML = title;
		btn.classList.add( htmlclass );
		btn.tabIndex = 1;
		btn.onclick = () => callback();
		wrap.append( btn );
	}

	protected _createWrap ( htmlclass: string, parent: Element ): void {
		const wrap = CE( 'div' );

		wrap.classList.add( htmlclass );
		parent.append( wrap );
	}
}
