export interface Data {
	parent: Element;
	data: string;
}

export interface Datas {
	parent: Element;
	datas: Array<{ key: string; value: string; }>;
}

export interface DataWithID {
	parent: Element;
	item: { id: string; key: string; value: string; };
}

export interface DatasWithID {
	parent: Element;
	datas: Array<{ id: string; key: string; value: string; }>;
}

export interface ActionTable {
	elder: Element;
	datas: Array<{ key: string; value: string; id: string; }>;
	identifier: string;
}

export interface ActionButton {
	parent: Element;
	callback: any;
	id: string;
	title: string;
	htmlclass: string;
}

export interface Result {
	parent: Element;
	count: number;
}
