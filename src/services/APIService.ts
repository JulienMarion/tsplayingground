import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import constants from './constants';

const { BASE_API_URL } = constants;

class APIService {
	private instance: AxiosInstance;

	constructor() {
		this.instance = axios.create( {
			baseURL: BASE_API_URL,
			headers: {
				'Content-Type': 'application/json'
			}
		} );
		this.instance.interceptors.response.use(
			( response: AxiosResponse ) => response,
			( error: AxiosError ) =>
				Promise.reject(
					( error.response && error.response.data ) || 'Something bad happened!'
				)
		);
	}

	public get = async ( params: string ) => (
		await this.instance.get( `/${ params }` )
	);

	public remove = async ( params: string ) => (
		await this.instance.delete( `/${ params }` )
	);

	public post = async ( params: string, data: {} ) => (
		await this.instance.post( `/${ params }`, data )
	);

	public patch = async ( params: string, data: {} ) => (
		await this.instance.patch( `/${ params }`, data )
	);

	public put = async ( params: string, data: {} ) => (
		await this.instance.put( `/${ params }`, data )
	);

	sayHello = () => console.log( 'HelloWorld' );
}

class instanceAPIService {
	private static instance: APIService;
	private constructor() { }

	public static getInstance () {

		if ( !this.instance ) { return new APIService(); }

		return this.instance;
	}
}

export default instanceAPIService.getInstance();
