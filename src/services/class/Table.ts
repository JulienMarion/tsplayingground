import { store } from '../../redux/store';
import { selectItem } from '../../redux/actions';
import { CE, QS } from '../../services/tools';
import DeleteModal from '../../services/modales/DeleteModal';
import FormModal from '../../services/modales/FormModal';
import {
	ActionButton,
	ActionTable,
	Data,
	DataWithID,
	Datas,
	DatasWithID,
	Result
} from '../../services/interfaces/interface';

export default class Table {
	private _createHeaders = ( { parent, data }: Data ): void => {
		const header = CE( 'th' );

		header.innerText = data;
		parent.appendChild( header );
	};

	private _createCell = ( parent: Element, { data }: Data ): void => {
		const cell = CE( 'td' );

		cell.innerHTML = data;
		parent.appendChild( cell );
	};

	private _handleModale = ( selector: any, id: string ): void => {

		if ( QS( `.${ selector.type }` ) ) { return selector.close(); }

		store.dispatch( selectItem( id ) );
		selector.open();
	};

	private _createRows = ( { parent, item }: DataWithID ): void => {
		const child = CE( 'tr' );

		for ( const [ keys, data ] of Object.entries( item ) ) {
			this._createCell( child, { parent, data } );
		}

		this._createActionBtn( {
			parent: child,
			callback: () => this._handleModale( FormModal, item.id ),
			id: item.id,
			title: 'edit',
			htmlclass: 'editBtn'
		} );

		this._createActionBtn( {
			parent: child,
			callback: () => this._handleModale( DeleteModal, item.id ),
			id: item.id,
			title: 'suppr',
			htmlclass: 'delBtn'
		} );

		parent.appendChild( child );
	};

	private _createResults = ( { parent, count }: Result ): void => {
		const results = CE( 'td' );

		results.setAttribute( 'colspan', '99' );
		results.style.textAlign = 'end';
		results.innerHTML = `Result count: ${ count }`;

		parent.appendChild( results );
	};

	private _fillRows = ( { parent, datas }: DatasWithID ): void[] => (
		datas.map( item => { this._createRows( { parent, item } ); } )
	);

	private _fillHeaders = ( { parent, datas }: Datas ): void => {

		for ( const data of Object.keys( datas[ 0 ] ) ) {

			this._createHeaders( { parent, data } );
		}
	};

	private _createActionBtn = ( {
		id,
		title,
		parent,
		callback,
		htmlclass
	}: ActionButton ): void => {

		const cell = CE( 'td' );

		cell.innerHTML = title;
		cell.classList.add( htmlclass );
		cell.onclick = () => callback( id );

		parent.appendChild( cell );
	};

	public _createTable = async ( {
		elder,
		datas,
		identifier
	}: ActionTable ): Promise<void> => {

		const parent = CE( 'table' );

		parent.id = identifier;

		this._fillHeaders( { parent, datas } );
		this._fillRows( { parent, datas } );
		this._createResults( { parent: parent, count: datas.length } );

		elder.appendChild( parent );
	};

	public static _deleteTable = ( identifier: string ): void => (
		QS( `#${ identifier }` ).remove()
	);
}
