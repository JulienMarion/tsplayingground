import { CE } from '../tools';

export class Form {
	constructor( public parent: Element, private template: {} ) { }

	public _generateForm () {
		const form = CE( 'form' );

		for ( const [ key, value ] of Object.entries( this.template ) ) {
			const wrap: HTMLElement = this._generateWrapper();
			const input: HTMLElement = this._generateInput( typeof value, key, value );
			const label: HTMLElement = this._generateLabel( key, input.id );

			input.tabIndex = 1;
			wrap.append( label );
			wrap.append( input );
			form.append( wrap );

			this.parent.append( form );
		}
	}

	private _generateWrapper () {
		const wrap = CE( 'div' );

		wrap.classList.add( 'formControl' );

		return wrap;
	}

	private _generateLabel ( title: string, htmlfor: string ) {
		const label = CE( 'label' );

		label.innerText = title;
		label.setAttribute( 'for', htmlfor );

		return label;
	}

	private _generateInput ( type: string, id: string, value: any ) {
		const input = <HTMLInputElement>CE( 'input' );
		input.id = id;

		if ( type === 'boolean' ) {
			input.setAttribute( 'type', 'checkbox' );
			input.checked = value;
			input.onchange = function ( e ) {
				const htmlTarget = <HTMLInputElement>e.target;
				input.checked = htmlTarget.checked;
			};

			return input;
		}

		input.setAttribute( 'type', type );
		input.setAttribute( 'value', value );
		input.onchange = function ( e ) {
			const htmlTarget = <HTMLInputElement>e.target;
			input.setAttribute( 'value', htmlTarget.value );
		};

		return input;
	}
}
