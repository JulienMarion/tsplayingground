import constants from '../services/constants';
import apiService from '../services/APIservice';
import { AxiosError, AxiosResponse } from 'axios';

const {
	PRODUCTS,
	ACTIONS: {
		GET_ITEMS,
		SELECT_ITEM,
		DELETE_ITEM,
		GET_ITEM,
		UPDATE_ITEM
	},

} = constants;

export const getItems = () => ( {
	type: GET_ITEMS,
	payload: apiService
		.get( PRODUCTS )
		.then( ( res: AxiosResponse ) => res.data )
		.catch( ( err: AxiosError ) => console.error( err ) )
} );

export const deleteItem = ( id: string ) => ( {
	type: DELETE_ITEM,
	payload: apiService
		.remove( `${ PRODUCTS }/${ id }` )
		.catch( ( err: AxiosError ) => console.log( err ) )
} );

export const selectItem = ( id: string ) => ( {
	type: SELECT_ITEM,
	payload: id
} );

export const getFullItem = ( id: string ) => ( {
	type: GET_ITEM,
	payload: apiService
		.get( `${ PRODUCTS }/${ id }` )
		.then( ( res: AxiosResponse ) => res.data )
		.catch( ( err: AxiosError ) => console.log( err ) )
} );

export const updateItem = ( id: string, data: {} ) => ( {
	type: UPDATE_ITEM,
	payload: apiService
		.patch( `${ PRODUCTS }/${ id }`, { ...data } )
		.catch( ( err: AxiosError ) => console.log( err ) )
} );
