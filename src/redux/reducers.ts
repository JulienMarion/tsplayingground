import constants from '../services/constants';

const {
	ACTIONS: {
		GET_ITEMS,
		SELECT_ITEM,
		GET_ITEM
	}
} = constants;

export interface InitialState {
	items: Array<any>;
	selectedItem: string;
	item: any;
}

const initialState: InitialState = {
	items: [],
	selectedItem: '',
	item: {}
};

export const reducers = (
	state = initialState,
	action: { type: string; payload: any; }
) => {
	switch ( action.type ) {
		case GET_ITEMS:
			return {
				...state,
				items: action.payload
			};
		case SELECT_ITEM:
			return {
				...state,
				selectedItem: action.payload
			};
		case GET_ITEM:
			return {
				...state,
				item: action.payload
			};
		default:
			return { ...state };
	}
};
