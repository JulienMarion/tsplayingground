import style from './style.css';
import tables from './tables.css';
import modal from './modal.css';

export default {
	style,
	tables,
	modal
};

// Remember to import and register stylesheets here.
